'use strict';

/**
 * @file
 * Drupal Link plugin.
 *
 * @ignore
 */

(function ($, Drupal, drupalSettings, CKEDITOR) {
  function parseAttributes(editor, element) {
    var parsedAttributes = {};

    var domElement = element.$;
    var attribute = void 0;
    var attributeName = void 0;
    for (var attrIndex = 0; attrIndex < domElement.attributes.length; attrIndex++) {
      attribute = domElement.attributes.item(attrIndex);
      attributeName = attribute.nodeName.toLowerCase();
      // Ignore data-cke-* attributes; they're CKEditor internals.
      if (attributeName.indexOf('data-cke-') === 0) {
        continue;
      }
      // Store the value for this attribute, unless there's a data-cke-saved-
      // alternative for it, which will contain the quirk-free, original value.
      parsedAttributes[attributeName] = element.data('cke-saved-' + attributeName) || attribute.nodeValue;
    }

    // Remove any cke_* classes.
    if (parsedAttributes.class) {
      parsedAttributes.class = CKEDITOR.tools.trim(parsedAttributes.class.replace(/cke_\S+/, ''));
    }

    return parsedAttributes;
  }

  function getAttributes(editor, data) {
    var set = {};
    Object.keys(data || {}).forEach(function (attributeName) {
      set[attributeName] = data[attributeName];
    });

    // CKEditor tracks the *actual* saved href in a data-cke-saved-* attribute
    // to work around browser quirks. We need to update it.
    set['data-cke-saved-href'] = set.href;

    // Remove all attributes which are not currently set.
    var removed = {};
    Object.keys(set).forEach(function (s) {
      delete removed[s];
    });

    return {
      set: set,
      removed: CKEDITOR.tools.objectKeys(removed)
    };
  }

  var registeredLinkableWidgets = [];

  /**
   * Registers a widget name as linkable.
   *
   * @param {string} widgetName
   *   The name of the widget to register as linkable.
   */
  function registerLinkableWidget(widgetName) {
    registeredLinkableWidgets.push(widgetName);
  }

  /**
   * Gets the focused widget, if one of the registered linkable widget names.
   *
   * @param {CKEDITOR.editor} editor
   *   A CKEditor instance.
   *
   * @return {?CKEDITOR.plugins.widget}
   *   The focused linkable widget instance, or null.
   */
  function getFocusedLinkableWidget(editor) {
    var widget = editor.widgets.focused;
    if (widget && registeredLinkableWidgets.indexOf(widget.name) !== -1) {
      return widget;
    }
    return null;
  }

  /**
   * Get the surrounding link element of current selection.
   *
   * The following selection will all return the link element.
   *
   * @example
   *  <a href="#">li^nk</a>
   *  <a href="#">[link]</a>
   *  text[<a href="#">link]</a>
   *  <a href="#">li[nk</a>]
   *  [<b><a href="#">li]nk</a></b>]
   *  [<a href="#"><b>li]nk</b></a>
   *
   * @param {CKEDITOR.editor} editor
   *   The CKEditor editor object
   *
   * @return {?HTMLElement}
   *   The selected link element, or null.
   *
   */
  function getSelectedLink(editor) {
    var selection = editor.getSelection();
    var selectedElement = selection.getSelectedElement();
    if (selectedElement && selectedElement.is('a')) {
      return selectedElement;
    }

    var range = selection.getRanges(true)[0];

    if (range) {
      range.shrink(CKEDITOR.SHRINK_TEXT);
      return editor.elementPath(range.getCommonAncestor()).contains('a', 1);
    }
    return null;
  }

  CKEDITOR.plugins.add('chainlink', {
    requires: 'link,fakeobjects',
    icons: 'chainlink',
    hidpi: true,

    init: function init(editor) {
      // Add the commands for link and unlink.
      editor.addCommand('chainlink', {
        allowedContent: {
          a: {
            attributes: {
              '!href': true
            },
            classes: {}
          }
        },
        requiredContent: new CKEDITOR.style({
          element: 'a',
          attributes: {
            href: ''
          }
        }),
        modes: { wysiwyg: 1 },
        canUndo: true,
        exec: function exec(editor) {
          var focusedLinkableWidget = getFocusedLinkableWidget(editor);
          var linkElement = getSelectedLink(editor);
          var linkText = '';
          // Set existing values based on selected element.
          var existingValues = {};
          if (linkElement && linkElement.$) {
            existingValues = parseAttributes(editor, linkElement);
            existingValues.selectedText = linkElement.$.innerText;
          }
          // Or, if an image widget is focused, we're editing a link wrapping
          // an image widget.
          else if (focusedLinkableWidget && focusedLinkableWidget.data.link) {
              existingValues = CKEDITOR.tools.clone(focusedLinkableWidget.data.link);
            }
            // Or we're creating a new link.
            else if (typeof editor.getSelection().getSelectedText() == 'string') {
                existingValues.selectedText = editor.getSelection().getSelectedText();
              }

          // Retrieve anchors from the link plugin.
          var linkPlugin = CKEDITOR.plugins.link;
          existingValues.anchors = linkPlugin.getEditorAnchors(editor);

          // Prepare a save callback to be used upon saving the dialog.
          var saveCallback = function saveCallback(returnValues) {
            // If an image widget is focused, we're not editing an independent
            // link, but we're wrapping an image widget in a link.
            if (focusedLinkableWidget) {
              focusedLinkableWidget.setData('link', CKEDITOR.tools.extend(returnValues.attributes, focusedLinkableWidget.data.link));
              editor.fire('saveSnapshot');
              return;
            }

            editor.fire('saveSnapshot');

            // Shorten mailto and tel urls for use as link text if none is provided.
            var linkUrl = returnValues.attributes.hasOwnProperty('href') ? returnValues.attributes.href.replace(/^mailto:/, '').replace(/^tel:/, '') : '';
            var submittedLinkText = returnValues.hasOwnProperty('linkText') && returnValues.linkText !== '' ? returnValues.linkText : linkUrl;

            // Create a new link element if needed.
            if (!linkElement && returnValues.attributes.href) {
              var selection = editor.getSelection();
              var range = selection.getRanges(1)[0];

              // Use link URL as text with a collapsed cursor.
              if (range.collapsed) {
                //selection start=end
                var text = new CKEDITOR.dom.text(submittedLinkText, editor.document);
                range.insertNode(text);
                range.selectNodeContents(text);
              }

              // Create the new link by applying a style to the new text.
              var style = new CKEDITOR.style({
                element: 'a',
                attributes: returnValues.attributes
              });
              style.type = CKEDITOR.STYLE_INLINE;
              style.applyToRange(range);
              range.select();

              // Set the link so individual properties may be set below.
              linkElement = getSelectedLink(editor);
            }
            // Update the link properties.
            else if (linkElement) {
                Object.keys(returnValues.attributes || {}).forEach(function (attrName) {
                  // Update the property if a value is specified.
                  if (returnValues.attributes[attrName].length > 0) {
                    var value = returnValues.attributes[attrName];
                    linkElement.data('cke-saved-' + attrName, value);
                    linkElement.setAttribute(attrName, value);
                  }
                  // Delete the property if set to an empty string.
                  else {
                      linkElement.removeAttribute(attrName);
                    }
                  // Update the link text if changed.
                  var isDisplayChanged = submittedLinkText && submittedLinkText !== linkElement.getHtml();
                  if (isDisplayChanged) {
                    linkElement.setHtml(submittedLinkText);
                  }
                });
              }

            // Save snapshot for undo support.
            editor.fire('saveSnapshot');
          };
          // Drupal.t() will not work inside CKEditor plugins because CKEditor
          // loads the JavaScript file instead of Drupal. Pull translated
          // strings from the plugin settings that are translated server-side.
          var dialogSettings = {
            title: linkElement ? editor.config.chainlink_dialogTitleEdit : editor.config.chainlink_dialogTitleAdd,
            dialogClass: 'editor-chainlink-dialog'
          };

          // Open the dialog for the edit form.
          Drupal.ckeditor.openDialog(editor, Drupal.url('editor/dialog/chainlink/' + editor.config.drupal.format), existingValues, saveCallback, dialogSettings);
        }
      });
      // editor.addCommand('drupalunlink', {
      //   contextSensitive: 1,
      //   startDisabled: 1,
      //   requiredContent: new CKEDITOR.style({
      //     element: 'a',
      //     attributes: {
      //       href: '',
      //     },
      //   }),
      //   exec(editor) {
      //     const style = new CKEDITOR.style({
      //       element: 'a',
      //       type: CKEDITOR.STYLE_INLINE,
      //       alwaysRemoveElement: 1,
      //     });
      //     editor.removeStyle(style);
      //   },
      //   refresh(editor, path) {
      //     const element =
      //       path.lastElement && path.lastElement.getAscendant('a', true);
      //     if (
      //       element &&
      //       element.getName() === 'a' &&
      //       element.getAttribute('href') &&
      //       element.getChildCount()
      //     ) {
      //       this.setState(CKEDITOR.TRISTATE_OFF);
      //     } else {
      //       this.setState(CKEDITOR.TRISTATE_DISABLED);
      //     }
      //   },
      // });

      // CTRL + K.
      editor.setKeystroke(CKEDITOR.CTRL + 75, 'chainlink');

      // Add buttons for link and unlink.
      if (editor.ui.addButton) {
        editor.ui.addButton('Chainlink', {
          label: Drupal.t('Link'),
          command: 'chainlink'
        });
        // editor.ui.addButton('DrupalUnlink', {
        //   label: Drupal.t('Unlink'),
        //   command: 'drupalunlink',
        // });
      }

      editor.on('doubleclick', function (evt) {
        var element = getSelectedLink(editor) || evt.data.element;

        if (!element.isReadOnly()) {
          if (element.is('a')) {
            editor.getSelection().selectElement(element);
            editor.getCommand('chainlink').exec();
            if (element.getAttribute('name') && (!element.getAttribute('href') || !element.getChildCount())) {
              // This is an anchor, do nothing.
            } else {
              // Prevent other link dialogs.
              evt.stop();
            }
          }
        }
      }, null, null, -10);

      // Add the menu group.
      editor.addMenuGroup('chainlink');
      // If the "menu" plugin is loaded, register the menu items.
      if (editor.addMenuItems) {
        // Remove the link menu items causign duplicates.
        // TODO: https://www.drupal.org/comment/reply/3131287
        editor.removeMenuItem('link');
        editor.removeMenuItem('unlink');

        editor.addMenuItems({
          chainlink: {
            label: Drupal.t('Edit Link'),
            command: 'chainlink',
            group: 'chainlink',
            order: 1
          },
          unchainlink: {
            label: Drupal.t('Unlink'),
            command: 'unlink',
            group: 'chainlink',
            order: 5
          }
        });
      }

      // If the "contextmenu" plugin is loaded, register the listeners.
      if (editor.contextMenu) {
        editor.contextMenu.addListener(function (element, selection) {
          if (!element || element.isReadOnly()) {
            return null;
          }
          var anchor = getSelectedLink(editor);
          if (!anchor) {
            return null;
          }

          var menu = {};
          if (anchor.getAttribute('href') && anchor.getChildCount()) {
            menu = {
              chainlink: CKEDITOR.TRISTATE_OFF,
              unchainlink: CKEDITOR.TRISTATE_OFF
            };
          }
          return menu;
        });
      }
    }
  });
  CKEDITOR.on('instanceReady', function (e) {});
  // Expose an API for other plugins to interact with chainlink widgets.
  // (Compatible with the official CKEditor link plugin's API:
  // http://dev.ckeditor.com/ticket/13885.)
  CKEDITOR.plugins.chainlink = {
    parseLinkAttributes: parseAttributes,
    getLinkAttributes: getAttributes,
    registerLinkableWidget: registerLinkableWidget
  };
})(jQuery, Drupal, drupalSettings, CKEDITOR);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNoYWlubGluay9wbHVnaW4uZXM2LmpzIl0sIm5hbWVzIjpbIiQiLCJEcnVwYWwiLCJkcnVwYWxTZXR0aW5ncyIsIkNLRURJVE9SIiwicGFyc2VBdHRyaWJ1dGVzIiwiZWRpdG9yIiwiZWxlbWVudCIsInBhcnNlZEF0dHJpYnV0ZXMiLCJkb21FbGVtZW50IiwiYXR0cmlidXRlIiwiYXR0cmlidXRlTmFtZSIsImF0dHJJbmRleCIsImF0dHJpYnV0ZXMiLCJsZW5ndGgiLCJpdGVtIiwibm9kZU5hbWUiLCJ0b0xvd2VyQ2FzZSIsImluZGV4T2YiLCJkYXRhIiwibm9kZVZhbHVlIiwiY2xhc3MiLCJ0b29scyIsInRyaW0iLCJyZXBsYWNlIiwiZ2V0QXR0cmlidXRlcyIsInNldCIsIk9iamVjdCIsImtleXMiLCJmb3JFYWNoIiwiaHJlZiIsInJlbW92ZWQiLCJzIiwib2JqZWN0S2V5cyIsInJlZ2lzdGVyZWRMaW5rYWJsZVdpZGdldHMiLCJyZWdpc3RlckxpbmthYmxlV2lkZ2V0Iiwid2lkZ2V0TmFtZSIsInB1c2giLCJnZXRGb2N1c2VkTGlua2FibGVXaWRnZXQiLCJ3aWRnZXQiLCJ3aWRnZXRzIiwiZm9jdXNlZCIsIm5hbWUiLCJnZXRTZWxlY3RlZExpbmsiLCJzZWxlY3Rpb24iLCJnZXRTZWxlY3Rpb24iLCJzZWxlY3RlZEVsZW1lbnQiLCJnZXRTZWxlY3RlZEVsZW1lbnQiLCJpcyIsInJhbmdlIiwiZ2V0UmFuZ2VzIiwic2hyaW5rIiwiU0hSSU5LX1RFWFQiLCJlbGVtZW50UGF0aCIsImdldENvbW1vbkFuY2VzdG9yIiwiY29udGFpbnMiLCJwbHVnaW5zIiwiYWRkIiwicmVxdWlyZXMiLCJpY29ucyIsImhpZHBpIiwiaW5pdCIsImFkZENvbW1hbmQiLCJhbGxvd2VkQ29udGVudCIsImEiLCJjbGFzc2VzIiwicmVxdWlyZWRDb250ZW50Iiwic3R5bGUiLCJtb2RlcyIsInd5c2l3eWciLCJjYW5VbmRvIiwiZXhlYyIsImZvY3VzZWRMaW5rYWJsZVdpZGdldCIsImxpbmtFbGVtZW50IiwibGlua1RleHQiLCJleGlzdGluZ1ZhbHVlcyIsInNlbGVjdGVkVGV4dCIsImlubmVyVGV4dCIsImxpbmsiLCJjbG9uZSIsImdldFNlbGVjdGVkVGV4dCIsImxpbmtQbHVnaW4iLCJhbmNob3JzIiwiZ2V0RWRpdG9yQW5jaG9ycyIsInNhdmVDYWxsYmFjayIsInJldHVyblZhbHVlcyIsInNldERhdGEiLCJleHRlbmQiLCJmaXJlIiwibGlua1VybCIsImhhc093blByb3BlcnR5Iiwic3VibWl0dGVkTGlua1RleHQiLCJjb2xsYXBzZWQiLCJ0ZXh0IiwiZG9tIiwiZG9jdW1lbnQiLCJpbnNlcnROb2RlIiwic2VsZWN0Tm9kZUNvbnRlbnRzIiwidHlwZSIsIlNUWUxFX0lOTElORSIsImFwcGx5VG9SYW5nZSIsInNlbGVjdCIsImF0dHJOYW1lIiwidmFsdWUiLCJzZXRBdHRyaWJ1dGUiLCJyZW1vdmVBdHRyaWJ1dGUiLCJpc0Rpc3BsYXlDaGFuZ2VkIiwiZ2V0SHRtbCIsInNldEh0bWwiLCJkaWFsb2dTZXR0aW5ncyIsInRpdGxlIiwiY29uZmlnIiwiY2hhaW5saW5rX2RpYWxvZ1RpdGxlRWRpdCIsImNoYWlubGlua19kaWFsb2dUaXRsZUFkZCIsImRpYWxvZ0NsYXNzIiwiY2tlZGl0b3IiLCJvcGVuRGlhbG9nIiwidXJsIiwiZHJ1cGFsIiwiZm9ybWF0Iiwic2V0S2V5c3Ryb2tlIiwiQ1RSTCIsInVpIiwiYWRkQnV0dG9uIiwibGFiZWwiLCJ0IiwiY29tbWFuZCIsIm9uIiwiZXZ0IiwiaXNSZWFkT25seSIsInNlbGVjdEVsZW1lbnQiLCJnZXRDb21tYW5kIiwiZ2V0QXR0cmlidXRlIiwiZ2V0Q2hpbGRDb3VudCIsInN0b3AiLCJhZGRNZW51R3JvdXAiLCJhZGRNZW51SXRlbXMiLCJyZW1vdmVNZW51SXRlbSIsImNoYWlubGluayIsImdyb3VwIiwib3JkZXIiLCJ1bmNoYWlubGluayIsImNvbnRleHRNZW51IiwiYWRkTGlzdGVuZXIiLCJhbmNob3IiLCJtZW51IiwiVFJJU1RBVEVfT0ZGIiwiZSIsInBhcnNlTGlua0F0dHJpYnV0ZXMiLCJnZXRMaW5rQXR0cmlidXRlcyIsImpRdWVyeSJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7OztBQU9BLENBQUMsVUFBU0EsQ0FBVCxFQUFZQyxNQUFaLEVBQW9CQyxjQUFwQixFQUFvQ0MsUUFBcEMsRUFBOEM7QUFDN0MsV0FBU0MsZUFBVCxDQUF5QkMsTUFBekIsRUFBaUNDLE9BQWpDLEVBQTBDO0FBQ3hDLFFBQU1DLG1CQUFtQixFQUF6Qjs7QUFFQSxRQUFNQyxhQUFhRixRQUFRTixDQUEzQjtBQUNBLFFBQUlTLGtCQUFKO0FBQ0EsUUFBSUMsc0JBQUo7QUFDQSxTQUNFLElBQUlDLFlBQVksQ0FEbEIsRUFFRUEsWUFBWUgsV0FBV0ksVUFBWCxDQUFzQkMsTUFGcEMsRUFHRUYsV0FIRixFQUlFO0FBQ0FGLGtCQUFZRCxXQUFXSSxVQUFYLENBQXNCRSxJQUF0QixDQUEyQkgsU0FBM0IsQ0FBWjtBQUNBRCxzQkFBZ0JELFVBQVVNLFFBQVYsQ0FBbUJDLFdBQW5CLEVBQWhCO0FBQ0E7QUFDQSxVQUFJTixjQUFjTyxPQUFkLENBQXNCLFdBQXRCLE1BQXVDLENBQTNDLEVBQThDO0FBQzVDO0FBQ0Q7QUFDRDtBQUNBO0FBQ0FWLHVCQUFpQkcsYUFBakIsSUFDRUosUUFBUVksSUFBUixnQkFBMEJSLGFBQTFCLEtBQThDRCxVQUFVVSxTQUQxRDtBQUVEOztBQUVEO0FBQ0EsUUFBSVosaUJBQWlCYSxLQUFyQixFQUE0QjtBQUMxQmIsdUJBQWlCYSxLQUFqQixHQUF5QmpCLFNBQVNrQixLQUFULENBQWVDLElBQWYsQ0FDdkJmLGlCQUFpQmEsS0FBakIsQ0FBdUJHLE9BQXZCLENBQStCLFNBQS9CLEVBQTBDLEVBQTFDLENBRHVCLENBQXpCO0FBR0Q7O0FBRUQsV0FBT2hCLGdCQUFQO0FBQ0Q7O0FBRUQsV0FBU2lCLGFBQVQsQ0FBdUJuQixNQUF2QixFQUErQmEsSUFBL0IsRUFBcUM7QUFDbkMsUUFBTU8sTUFBTSxFQUFaO0FBQ0FDLFdBQU9DLElBQVAsQ0FBWVQsUUFBUSxFQUFwQixFQUF3QlUsT0FBeEIsQ0FBZ0MseUJBQWlCO0FBQy9DSCxVQUFJZixhQUFKLElBQXFCUSxLQUFLUixhQUFMLENBQXJCO0FBQ0QsS0FGRDs7QUFJQTtBQUNBO0FBQ0FlLFFBQUkscUJBQUosSUFBNkJBLElBQUlJLElBQWpDOztBQUVBO0FBQ0EsUUFBTUMsVUFBVSxFQUFoQjtBQUNBSixXQUFPQyxJQUFQLENBQVlGLEdBQVosRUFBaUJHLE9BQWpCLENBQXlCLGFBQUs7QUFDNUIsYUFBT0UsUUFBUUMsQ0FBUixDQUFQO0FBQ0QsS0FGRDs7QUFJQSxXQUFPO0FBQ0xOLGNBREs7QUFFTEssZUFBUzNCLFNBQVNrQixLQUFULENBQWVXLFVBQWYsQ0FBMEJGLE9BQTFCO0FBRkosS0FBUDtBQUlEOztBQUVELE1BQU1HLDRCQUE0QixFQUFsQzs7QUFFQTs7Ozs7O0FBTUEsV0FBU0Msc0JBQVQsQ0FBZ0NDLFVBQWhDLEVBQTRDO0FBQzFDRiw4QkFBMEJHLElBQTFCLENBQStCRCxVQUEvQjtBQUNEOztBQUVEOzs7Ozs7Ozs7QUFTQSxXQUFTRSx3QkFBVCxDQUFrQ2hDLE1BQWxDLEVBQTBDO0FBQ3hDLFFBQU1pQyxTQUFTakMsT0FBT2tDLE9BQVAsQ0FBZUMsT0FBOUI7QUFDQSxRQUFJRixVQUFVTCwwQkFBMEJoQixPQUExQixDQUFrQ3FCLE9BQU9HLElBQXpDLE1BQW1ELENBQUMsQ0FBbEUsRUFBcUU7QUFDbkUsYUFBT0gsTUFBUDtBQUNEO0FBQ0QsV0FBTyxJQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBb0JBLFdBQVNJLGVBQVQsQ0FBeUJyQyxNQUF6QixFQUFpQztBQUMvQixRQUFNc0MsWUFBWXRDLE9BQU91QyxZQUFQLEVBQWxCO0FBQ0EsUUFBTUMsa0JBQWtCRixVQUFVRyxrQkFBVixFQUF4QjtBQUNBLFFBQUlELG1CQUFtQkEsZ0JBQWdCRSxFQUFoQixDQUFtQixHQUFuQixDQUF2QixFQUFnRDtBQUM5QyxhQUFPRixlQUFQO0FBQ0Q7O0FBRUQsUUFBTUcsUUFBUUwsVUFBVU0sU0FBVixDQUFvQixJQUFwQixFQUEwQixDQUExQixDQUFkOztBQUVBLFFBQUlELEtBQUosRUFBVztBQUNUQSxZQUFNRSxNQUFOLENBQWEvQyxTQUFTZ0QsV0FBdEI7QUFDQSxhQUFPOUMsT0FBTytDLFdBQVAsQ0FBbUJKLE1BQU1LLGlCQUFOLEVBQW5CLEVBQThDQyxRQUE5QyxDQUF1RCxHQUF2RCxFQUE0RCxDQUE1RCxDQUFQO0FBQ0Q7QUFDRCxXQUFPLElBQVA7QUFDRDs7QUFFRG5ELFdBQVNvRCxPQUFULENBQWlCQyxHQUFqQixDQUFxQixXQUFyQixFQUFrQztBQUNoQ0MsY0FBVSxrQkFEc0I7QUFFaENDLFdBQU8sV0FGeUI7QUFHaENDLFdBQU8sSUFIeUI7O0FBS2hDQyxRQUxnQyxnQkFLM0J2RCxNQUwyQixFQUtuQjtBQUNYO0FBQ0FBLGFBQU93RCxVQUFQLENBQWtCLFdBQWxCLEVBQStCO0FBQzdCQyx3QkFBZ0I7QUFDZEMsYUFBRztBQUNEbkQsd0JBQVk7QUFDVix1QkFBUztBQURDLGFBRFg7QUFJRG9ELHFCQUFTO0FBSlI7QUFEVyxTQURhO0FBUzdCQyx5QkFBaUIsSUFBSTlELFNBQVMrRCxLQUFiLENBQW1CO0FBQ2xDNUQsbUJBQVMsR0FEeUI7QUFFbENNLHNCQUFZO0FBQ1ZpQixrQkFBTTtBQURJO0FBRnNCLFNBQW5CLENBVFk7QUFlN0JzQyxlQUFPLEVBQUVDLFNBQVMsQ0FBWCxFQWZzQjtBQWdCN0JDLGlCQUFTLElBaEJvQjtBQWlCN0JDLFlBakI2QixnQkFpQnhCakUsTUFqQndCLEVBaUJoQjtBQUNYLGNBQU1rRSx3QkFBd0JsQyx5QkFBeUJoQyxNQUF6QixDQUE5QjtBQUNBLGNBQUltRSxjQUFjOUIsZ0JBQWdCckMsTUFBaEIsQ0FBbEI7QUFDQSxjQUFJb0UsV0FBVyxFQUFmO0FBQ0E7QUFDQSxjQUFJQyxpQkFBaUIsRUFBckI7QUFDQSxjQUFJRixlQUFlQSxZQUFZeEUsQ0FBL0IsRUFBa0M7QUFDaEMwRSw2QkFBaUJ0RSxnQkFBZ0JDLE1BQWhCLEVBQXdCbUUsV0FBeEIsQ0FBakI7QUFDQUUsMkJBQWVDLFlBQWYsR0FBOEJILFlBQVl4RSxDQUFaLENBQWM0RSxTQUE1QztBQUNEO0FBQ0Q7QUFDQTtBQUxBLGVBTUssSUFBSUwseUJBQXlCQSxzQkFBc0JyRCxJQUF0QixDQUEyQjJELElBQXhELEVBQThEO0FBQ2pFSCwrQkFBaUJ2RSxTQUFTa0IsS0FBVCxDQUFleUQsS0FBZixDQUNmUCxzQkFBc0JyRCxJQUF0QixDQUEyQjJELElBRFosQ0FBakI7QUFHRDtBQUNEO0FBTEssaUJBTUEsSUFBSSxPQUFPeEUsT0FBT3VDLFlBQVAsR0FBc0JtQyxlQUF0QixFQUFQLElBQWtELFFBQXRELEVBQWdFO0FBQ25FTCwrQkFBZUMsWUFBZixHQUE4QnRFLE9BQU91QyxZQUFQLEdBQXNCbUMsZUFBdEIsRUFBOUI7QUFDRDs7QUFFRDtBQUNBLGNBQUlDLGFBQWE3RSxTQUFTb0QsT0FBVCxDQUFpQnNCLElBQWxDO0FBQ0FILHlCQUFlTyxPQUFmLEdBQXlCRCxXQUFXRSxnQkFBWCxDQUE0QjdFLE1BQTVCLENBQXpCOztBQUVBO0FBQ0EsY0FBTThFLGVBQWUsU0FBZkEsWUFBZSxDQUFTQyxZQUFULEVBQXVCO0FBQzFDO0FBQ0E7QUFDQSxnQkFBSWIscUJBQUosRUFBMkI7QUFDekJBLG9DQUFzQmMsT0FBdEIsQ0FDRSxNQURGLEVBRUVsRixTQUFTa0IsS0FBVCxDQUFlaUUsTUFBZixDQUNFRixhQUFheEUsVUFEZixFQUVFMkQsc0JBQXNCckQsSUFBdEIsQ0FBMkIyRCxJQUY3QixDQUZGO0FBT0F4RSxxQkFBT2tGLElBQVAsQ0FBWSxjQUFaO0FBQ0E7QUFDRDs7QUFFRGxGLG1CQUFPa0YsSUFBUCxDQUFZLGNBQVo7O0FBRUE7QUFDQSxnQkFBTUMsVUFBVUosYUFBYXhFLFVBQWIsQ0FBd0I2RSxjQUF4QixDQUF1QyxNQUF2QyxJQUNaTCxhQUFheEUsVUFBYixDQUF3QmlCLElBQXhCLENBQTZCTixPQUE3QixDQUFxQyxVQUFyQyxFQUFpRCxFQUFqRCxFQUFxREEsT0FBckQsQ0FBNkQsT0FBN0QsRUFBc0UsRUFBdEUsQ0FEWSxHQUVaLEVBRko7QUFHQSxnQkFBTW1FLG9CQUFvQk4sYUFBYUssY0FBYixDQUE0QixVQUE1QixLQUEyQ0wsYUFBYVgsUUFBYixLQUEwQixFQUFyRSxHQUN0QlcsYUFBYVgsUUFEUyxHQUV0QmUsT0FGSjs7QUFJQTtBQUNBLGdCQUFJLENBQUNoQixXQUFELElBQWdCWSxhQUFheEUsVUFBYixDQUF3QmlCLElBQTVDLEVBQWtEO0FBQ2hELGtCQUFNYyxZQUFZdEMsT0FBT3VDLFlBQVAsRUFBbEI7QUFDQSxrQkFBTUksUUFBUUwsVUFBVU0sU0FBVixDQUFvQixDQUFwQixFQUF1QixDQUF2QixDQUFkOztBQUVBO0FBQ0Esa0JBQUlELE1BQU0yQyxTQUFWLEVBQXFCO0FBQUU7QUFDckIsb0JBQU1DLE9BQU8sSUFBSXpGLFNBQVMwRixHQUFULENBQWFELElBQWpCLENBQ1hGLGlCQURXLEVBRVhyRixPQUFPeUYsUUFGSSxDQUFiO0FBSUE5QyxzQkFBTStDLFVBQU4sQ0FBaUJILElBQWpCO0FBQ0E1QyxzQkFBTWdELGtCQUFOLENBQXlCSixJQUF6QjtBQUNEOztBQUVEO0FBQ0Esa0JBQU0xQixRQUFRLElBQUkvRCxTQUFTK0QsS0FBYixDQUFtQjtBQUMvQjVELHlCQUFTLEdBRHNCO0FBRS9CTSw0QkFBWXdFLGFBQWF4RTtBQUZNLGVBQW5CLENBQWQ7QUFJQXNELG9CQUFNK0IsSUFBTixHQUFhOUYsU0FBUytGLFlBQXRCO0FBQ0FoQyxvQkFBTWlDLFlBQU4sQ0FBbUJuRCxLQUFuQjtBQUNBQSxvQkFBTW9ELE1BQU47O0FBRUE7QUFDQTVCLDRCQUFjOUIsZ0JBQWdCckMsTUFBaEIsQ0FBZDtBQUNEO0FBQ0Q7QUExQkEsaUJBMkJLLElBQUltRSxXQUFKLEVBQWlCO0FBQ3BCOUMsdUJBQU9DLElBQVAsQ0FBWXlELGFBQWF4RSxVQUFiLElBQTJCLEVBQXZDLEVBQTJDZ0IsT0FBM0MsQ0FBbUQsb0JBQVk7QUFDN0Q7QUFDQSxzQkFBSXdELGFBQWF4RSxVQUFiLENBQXdCeUYsUUFBeEIsRUFBa0N4RixNQUFsQyxHQUEyQyxDQUEvQyxFQUFrRDtBQUNoRCx3QkFBTXlGLFFBQVFsQixhQUFheEUsVUFBYixDQUF3QnlGLFFBQXhCLENBQWQ7QUFDQTdCLGdDQUFZdEQsSUFBWixnQkFBOEJtRixRQUE5QixFQUEwQ0MsS0FBMUM7QUFDQTlCLGdDQUFZK0IsWUFBWixDQUF5QkYsUUFBekIsRUFBbUNDLEtBQW5DO0FBQ0Q7QUFDRDtBQUxBLHVCQU1LO0FBQ0g5QixrQ0FBWWdDLGVBQVosQ0FBNEJILFFBQTVCO0FBQ0Q7QUFDRDtBQUNBLHNCQUFNSSxtQkFBbUJmLHFCQUFxQkEsc0JBQXNCbEIsWUFBWWtDLE9BQVosRUFBcEU7QUFDQSxzQkFBSUQsZ0JBQUosRUFBc0I7QUFDckJqQyxnQ0FBWW1DLE9BQVosQ0FBb0JqQixpQkFBcEI7QUFDQTtBQUNGLGlCQWhCRDtBQWlCRDs7QUFFRDtBQUNBckYsbUJBQU9rRixJQUFQLENBQVksY0FBWjtBQUNELFdBM0VEO0FBNEVBO0FBQ0E7QUFDQTtBQUNBLGNBQU1xQixpQkFBaUI7QUFDckJDLG1CQUFPckMsY0FDSG5FLE9BQU95RyxNQUFQLENBQWNDLHlCQURYLEdBRUgxRyxPQUFPeUcsTUFBUCxDQUFjRSx3QkFIRztBQUlyQkMseUJBQWE7QUFKUSxXQUF2Qjs7QUFPQTtBQUNBaEgsaUJBQU9pSCxRQUFQLENBQWdCQyxVQUFoQixDQUNFOUcsTUFERixFQUVFSixPQUFPbUgsR0FBUCw4QkFBc0MvRyxPQUFPeUcsTUFBUCxDQUFjTyxNQUFkLENBQXFCQyxNQUEzRCxDQUZGLEVBR0U1QyxjQUhGLEVBSUVTLFlBSkYsRUFLRXlCLGNBTEY7QUFPRDtBQTFJNEIsT0FBL0I7QUE0SUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBdkcsYUFBT2tILFlBQVAsQ0FBb0JwSCxTQUFTcUgsSUFBVCxHQUFnQixFQUFwQyxFQUF3QyxXQUF4Qzs7QUFFQTtBQUNBLFVBQUluSCxPQUFPb0gsRUFBUCxDQUFVQyxTQUFkLEVBQXlCO0FBQ3ZCckgsZUFBT29ILEVBQVAsQ0FBVUMsU0FBVixDQUFvQixXQUFwQixFQUFpQztBQUMvQkMsaUJBQU8xSCxPQUFPMkgsQ0FBUCxDQUFTLE1BQVQsQ0FEd0I7QUFFL0JDLG1CQUFTO0FBRnNCLFNBQWpDO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDRDs7QUFFRHhILGFBQU95SCxFQUFQLENBQVUsYUFBVixFQUF5QixlQUFPO0FBQzlCLFlBQU14SCxVQUFVb0MsZ0JBQWdCckMsTUFBaEIsS0FBMkIwSCxJQUFJN0csSUFBSixDQUFTWixPQUFwRDs7QUFFQSxZQUFJLENBQUNBLFFBQVEwSCxVQUFSLEVBQUwsRUFBMkI7QUFDekIsY0FBSTFILFFBQVF5QyxFQUFSLENBQVcsR0FBWCxDQUFKLEVBQXFCO0FBQ25CMUMsbUJBQU91QyxZQUFQLEdBQXNCcUYsYUFBdEIsQ0FBb0MzSCxPQUFwQztBQUNBRCxtQkFBTzZILFVBQVAsQ0FBa0IsV0FBbEIsRUFBK0I1RCxJQUEvQjtBQUNBLGdCQUFJaEUsUUFBUTZILFlBQVIsQ0FBc0IsTUFBdEIsTUFBb0MsQ0FBQzdILFFBQVE2SCxZQUFSLENBQXNCLE1BQXRCLENBQUQsSUFBbUMsQ0FBQzdILFFBQVE4SCxhQUFSLEVBQXhFLENBQUosRUFBd0c7QUFDdEc7QUFDRCxhQUZELE1BRU87QUFDTDtBQUNBTCxrQkFBSU0sSUFBSjtBQUNEO0FBQ0Y7QUFDRjtBQUNGLE9BZkQsRUFlRyxJQWZILEVBZVMsSUFmVCxFQWVlLENBQUMsRUFmaEI7O0FBaUJBO0FBQ0FoSSxhQUFPaUksWUFBUCxDQUFvQixXQUFwQjtBQUNBO0FBQ0EsVUFBSWpJLE9BQU9rSSxZQUFYLEVBQXlCO0FBQ3ZCO0FBQ0E7QUFDQWxJLGVBQU9tSSxjQUFQLENBQXNCLE1BQXRCO0FBQ0FuSSxlQUFPbUksY0FBUCxDQUFzQixRQUF0Qjs7QUFFQW5JLGVBQU9rSSxZQUFQLENBQW9CO0FBQ2xCRSxxQkFBVztBQUNUZCxtQkFBTzFILE9BQU8ySCxDQUFQLENBQVMsV0FBVCxDQURFO0FBRVRDLHFCQUFTLFdBRkE7QUFHVGEsbUJBQU8sV0FIRTtBQUlUQyxtQkFBTztBQUpFLFdBRE87QUFPbEJDLHVCQUFhO0FBQ1hqQixtQkFBTzFILE9BQU8ySCxDQUFQLENBQVMsUUFBVCxDQURJO0FBRVhDLHFCQUFTLFFBRkU7QUFHWGEsbUJBQU8sV0FISTtBQUlYQyxtQkFBTztBQUpJO0FBUEssU0FBcEI7QUFjRDs7QUFFRDtBQUNBLFVBQUl0SSxPQUFPd0ksV0FBWCxFQUF3QjtBQUN0QnhJLGVBQU93SSxXQUFQLENBQW1CQyxXQUFuQixDQUErQixVQUFDeEksT0FBRCxFQUFVcUMsU0FBVixFQUF3QjtBQUNyRCxjQUFJLENBQUNyQyxPQUFELElBQVlBLFFBQVEwSCxVQUFSLEVBQWhCLEVBQXNDO0FBQ3BDLG1CQUFPLElBQVA7QUFDRDtBQUNELGNBQU1lLFNBQVNyRyxnQkFBZ0JyQyxNQUFoQixDQUFmO0FBQ0EsY0FBSSxDQUFDMEksTUFBTCxFQUFhO0FBQ1gsbUJBQU8sSUFBUDtBQUNEOztBQUVELGNBQUlDLE9BQU8sRUFBWDtBQUNBLGNBQUlELE9BQU9aLFlBQVAsQ0FBb0IsTUFBcEIsS0FBK0JZLE9BQU9YLGFBQVAsRUFBbkMsRUFBMkQ7QUFDekRZLG1CQUFPO0FBQ0xQLHlCQUFXdEksU0FBUzhJLFlBRGY7QUFFTEwsMkJBQWF6SSxTQUFTOEk7QUFGakIsYUFBUDtBQUlEO0FBQ0QsaUJBQU9ELElBQVA7QUFDRCxTQWpCRDtBQWtCRDtBQUNGO0FBbFErQixHQUFsQztBQW9RQTdJLFdBQVMySCxFQUFULENBQVksZUFBWixFQUE2QixVQUFVb0IsQ0FBVixFQUFhLENBQ3pDLENBREQ7QUFFQTtBQUNBO0FBQ0E7QUFDQS9JLFdBQVNvRCxPQUFULENBQWlCa0YsU0FBakIsR0FBNkI7QUFDM0JVLHlCQUFxQi9JLGVBRE07QUFFM0JnSix1QkFBbUI1SCxhQUZRO0FBRzNCVTtBQUgyQixHQUE3QjtBQUtELENBdllELEVBdVlHbUgsTUF2WUgsRUF1WVdwSixNQXZZWCxFQXVZbUJDLGNBdlluQixFQXVZbUNDLFFBdlluQyIsImZpbGUiOiJjaGFpbmxpbmsvcGx1Z2luLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBAZmlsZVxuICogRHJ1cGFsIExpbmsgcGx1Z2luLlxuICpcbiAqIEBpZ25vcmVcbiAqL1xuXG4oZnVuY3Rpb24oJCwgRHJ1cGFsLCBkcnVwYWxTZXR0aW5ncywgQ0tFRElUT1IpIHtcbiAgZnVuY3Rpb24gcGFyc2VBdHRyaWJ1dGVzKGVkaXRvciwgZWxlbWVudCkge1xuICAgIGNvbnN0IHBhcnNlZEF0dHJpYnV0ZXMgPSB7fTtcblxuICAgIGNvbnN0IGRvbUVsZW1lbnQgPSBlbGVtZW50LiQ7XG4gICAgbGV0IGF0dHJpYnV0ZTtcbiAgICBsZXQgYXR0cmlidXRlTmFtZTtcbiAgICBmb3IgKFxuICAgICAgbGV0IGF0dHJJbmRleCA9IDA7XG4gICAgICBhdHRySW5kZXggPCBkb21FbGVtZW50LmF0dHJpYnV0ZXMubGVuZ3RoO1xuICAgICAgYXR0ckluZGV4KytcbiAgICApIHtcbiAgICAgIGF0dHJpYnV0ZSA9IGRvbUVsZW1lbnQuYXR0cmlidXRlcy5pdGVtKGF0dHJJbmRleCk7XG4gICAgICBhdHRyaWJ1dGVOYW1lID0gYXR0cmlidXRlLm5vZGVOYW1lLnRvTG93ZXJDYXNlKCk7XG4gICAgICAvLyBJZ25vcmUgZGF0YS1ja2UtKiBhdHRyaWJ1dGVzOyB0aGV5J3JlIENLRWRpdG9yIGludGVybmFscy5cbiAgICAgIGlmIChhdHRyaWJ1dGVOYW1lLmluZGV4T2YoJ2RhdGEtY2tlLScpID09PSAwKSB7XG4gICAgICAgIGNvbnRpbnVlO1xuICAgICAgfVxuICAgICAgLy8gU3RvcmUgdGhlIHZhbHVlIGZvciB0aGlzIGF0dHJpYnV0ZSwgdW5sZXNzIHRoZXJlJ3MgYSBkYXRhLWNrZS1zYXZlZC1cbiAgICAgIC8vIGFsdGVybmF0aXZlIGZvciBpdCwgd2hpY2ggd2lsbCBjb250YWluIHRoZSBxdWlyay1mcmVlLCBvcmlnaW5hbCB2YWx1ZS5cbiAgICAgIHBhcnNlZEF0dHJpYnV0ZXNbYXR0cmlidXRlTmFtZV0gPVxuICAgICAgICBlbGVtZW50LmRhdGEoYGNrZS1zYXZlZC0ke2F0dHJpYnV0ZU5hbWV9YCkgfHwgYXR0cmlidXRlLm5vZGVWYWx1ZTtcbiAgICB9XG5cbiAgICAvLyBSZW1vdmUgYW55IGNrZV8qIGNsYXNzZXMuXG4gICAgaWYgKHBhcnNlZEF0dHJpYnV0ZXMuY2xhc3MpIHtcbiAgICAgIHBhcnNlZEF0dHJpYnV0ZXMuY2xhc3MgPSBDS0VESVRPUi50b29scy50cmltKFxuICAgICAgICBwYXJzZWRBdHRyaWJ1dGVzLmNsYXNzLnJlcGxhY2UoL2NrZV9cXFMrLywgJycpLFxuICAgICAgKTtcbiAgICB9XG5cbiAgICByZXR1cm4gcGFyc2VkQXR0cmlidXRlcztcbiAgfVxuXG4gIGZ1bmN0aW9uIGdldEF0dHJpYnV0ZXMoZWRpdG9yLCBkYXRhKSB7XG4gICAgY29uc3Qgc2V0ID0ge307XG4gICAgT2JqZWN0LmtleXMoZGF0YSB8fCB7fSkuZm9yRWFjaChhdHRyaWJ1dGVOYW1lID0+IHtcbiAgICAgIHNldFthdHRyaWJ1dGVOYW1lXSA9IGRhdGFbYXR0cmlidXRlTmFtZV07XG4gICAgfSk7XG5cbiAgICAvLyBDS0VkaXRvciB0cmFja3MgdGhlICphY3R1YWwqIHNhdmVkIGhyZWYgaW4gYSBkYXRhLWNrZS1zYXZlZC0qIGF0dHJpYnV0ZVxuICAgIC8vIHRvIHdvcmsgYXJvdW5kIGJyb3dzZXIgcXVpcmtzLiBXZSBuZWVkIHRvIHVwZGF0ZSBpdC5cbiAgICBzZXRbJ2RhdGEtY2tlLXNhdmVkLWhyZWYnXSA9IHNldC5ocmVmO1xuXG4gICAgLy8gUmVtb3ZlIGFsbCBhdHRyaWJ1dGVzIHdoaWNoIGFyZSBub3QgY3VycmVudGx5IHNldC5cbiAgICBjb25zdCByZW1vdmVkID0ge307XG4gICAgT2JqZWN0LmtleXMoc2V0KS5mb3JFYWNoKHMgPT4ge1xuICAgICAgZGVsZXRlIHJlbW92ZWRbc107XG4gICAgfSk7XG5cbiAgICByZXR1cm4ge1xuICAgICAgc2V0LFxuICAgICAgcmVtb3ZlZDogQ0tFRElUT1IudG9vbHMub2JqZWN0S2V5cyhyZW1vdmVkKSxcbiAgICB9O1xuICB9XG5cbiAgY29uc3QgcmVnaXN0ZXJlZExpbmthYmxlV2lkZ2V0cyA9IFtdO1xuXG4gIC8qKlxuICAgKiBSZWdpc3RlcnMgYSB3aWRnZXQgbmFtZSBhcyBsaW5rYWJsZS5cbiAgICpcbiAgICogQHBhcmFtIHtzdHJpbmd9IHdpZGdldE5hbWVcbiAgICogICBUaGUgbmFtZSBvZiB0aGUgd2lkZ2V0IHRvIHJlZ2lzdGVyIGFzIGxpbmthYmxlLlxuICAgKi9cbiAgZnVuY3Rpb24gcmVnaXN0ZXJMaW5rYWJsZVdpZGdldCh3aWRnZXROYW1lKSB7XG4gICAgcmVnaXN0ZXJlZExpbmthYmxlV2lkZ2V0cy5wdXNoKHdpZGdldE5hbWUpO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldHMgdGhlIGZvY3VzZWQgd2lkZ2V0LCBpZiBvbmUgb2YgdGhlIHJlZ2lzdGVyZWQgbGlua2FibGUgd2lkZ2V0IG5hbWVzLlxuICAgKlxuICAgKiBAcGFyYW0ge0NLRURJVE9SLmVkaXRvcn0gZWRpdG9yXG4gICAqICAgQSBDS0VkaXRvciBpbnN0YW5jZS5cbiAgICpcbiAgICogQHJldHVybiB7P0NLRURJVE9SLnBsdWdpbnMud2lkZ2V0fVxuICAgKiAgIFRoZSBmb2N1c2VkIGxpbmthYmxlIHdpZGdldCBpbnN0YW5jZSwgb3IgbnVsbC5cbiAgICovXG4gIGZ1bmN0aW9uIGdldEZvY3VzZWRMaW5rYWJsZVdpZGdldChlZGl0b3IpIHtcbiAgICBjb25zdCB3aWRnZXQgPSBlZGl0b3Iud2lkZ2V0cy5mb2N1c2VkO1xuICAgIGlmICh3aWRnZXQgJiYgcmVnaXN0ZXJlZExpbmthYmxlV2lkZ2V0cy5pbmRleE9mKHdpZGdldC5uYW1lKSAhPT0gLTEpIHtcbiAgICAgIHJldHVybiB3aWRnZXQ7XG4gICAgfVxuICAgIHJldHVybiBudWxsO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldCB0aGUgc3Vycm91bmRpbmcgbGluayBlbGVtZW50IG9mIGN1cnJlbnQgc2VsZWN0aW9uLlxuICAgKlxuICAgKiBUaGUgZm9sbG93aW5nIHNlbGVjdGlvbiB3aWxsIGFsbCByZXR1cm4gdGhlIGxpbmsgZWxlbWVudC5cbiAgICpcbiAgICogQGV4YW1wbGVcbiAgICogIDxhIGhyZWY9XCIjXCI+bGlebms8L2E+XG4gICAqICA8YSBocmVmPVwiI1wiPltsaW5rXTwvYT5cbiAgICogIHRleHRbPGEgaHJlZj1cIiNcIj5saW5rXTwvYT5cbiAgICogIDxhIGhyZWY9XCIjXCI+bGlbbms8L2E+XVxuICAgKiAgWzxiPjxhIGhyZWY9XCIjXCI+bGldbms8L2E+PC9iPl1cbiAgICogIFs8YSBocmVmPVwiI1wiPjxiPmxpXW5rPC9iPjwvYT5cbiAgICpcbiAgICogQHBhcmFtIHtDS0VESVRPUi5lZGl0b3J9IGVkaXRvclxuICAgKiAgIFRoZSBDS0VkaXRvciBlZGl0b3Igb2JqZWN0XG4gICAqXG4gICAqIEByZXR1cm4gez9IVE1MRWxlbWVudH1cbiAgICogICBUaGUgc2VsZWN0ZWQgbGluayBlbGVtZW50LCBvciBudWxsLlxuICAgKlxuICAgKi9cbiAgZnVuY3Rpb24gZ2V0U2VsZWN0ZWRMaW5rKGVkaXRvcikge1xuICAgIGNvbnN0IHNlbGVjdGlvbiA9IGVkaXRvci5nZXRTZWxlY3Rpb24oKTtcbiAgICBjb25zdCBzZWxlY3RlZEVsZW1lbnQgPSBzZWxlY3Rpb24uZ2V0U2VsZWN0ZWRFbGVtZW50KCk7XG4gICAgaWYgKHNlbGVjdGVkRWxlbWVudCAmJiBzZWxlY3RlZEVsZW1lbnQuaXMoJ2EnKSkge1xuICAgICAgcmV0dXJuIHNlbGVjdGVkRWxlbWVudDtcbiAgICB9XG5cbiAgICBjb25zdCByYW5nZSA9IHNlbGVjdGlvbi5nZXRSYW5nZXModHJ1ZSlbMF07XG5cbiAgICBpZiAocmFuZ2UpIHtcbiAgICAgIHJhbmdlLnNocmluayhDS0VESVRPUi5TSFJJTktfVEVYVCk7XG4gICAgICByZXR1cm4gZWRpdG9yLmVsZW1lbnRQYXRoKHJhbmdlLmdldENvbW1vbkFuY2VzdG9yKCkpLmNvbnRhaW5zKCdhJywgMSk7XG4gICAgfVxuICAgIHJldHVybiBudWxsO1xuICB9XG5cbiAgQ0tFRElUT1IucGx1Z2lucy5hZGQoJ2NoYWlubGluaycsIHtcbiAgICByZXF1aXJlczogJ2xpbmssZmFrZW9iamVjdHMnLFxuICAgIGljb25zOiAnY2hhaW5saW5rJyxcbiAgICBoaWRwaTogdHJ1ZSxcblxuICAgIGluaXQoZWRpdG9yKSB7XG4gICAgICAvLyBBZGQgdGhlIGNvbW1hbmRzIGZvciBsaW5rIGFuZCB1bmxpbmsuXG4gICAgICBlZGl0b3IuYWRkQ29tbWFuZCgnY2hhaW5saW5rJywge1xuICAgICAgICBhbGxvd2VkQ29udGVudDoge1xuICAgICAgICAgIGE6IHtcbiAgICAgICAgICAgIGF0dHJpYnV0ZXM6IHtcbiAgICAgICAgICAgICAgJyFocmVmJzogdHJ1ZSxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBjbGFzc2VzOiB7fSxcbiAgICAgICAgICB9LFxuICAgICAgICB9LFxuICAgICAgICByZXF1aXJlZENvbnRlbnQ6IG5ldyBDS0VESVRPUi5zdHlsZSh7XG4gICAgICAgICAgZWxlbWVudDogJ2EnLFxuICAgICAgICAgIGF0dHJpYnV0ZXM6IHtcbiAgICAgICAgICAgIGhyZWY6ICcnLFxuICAgICAgICAgIH0sXG4gICAgICAgIH0pLFxuICAgICAgICBtb2RlczogeyB3eXNpd3lnOiAxIH0sXG4gICAgICAgIGNhblVuZG86IHRydWUsXG4gICAgICAgIGV4ZWMoZWRpdG9yKSB7XG4gICAgICAgICAgY29uc3QgZm9jdXNlZExpbmthYmxlV2lkZ2V0ID0gZ2V0Rm9jdXNlZExpbmthYmxlV2lkZ2V0KGVkaXRvcik7XG4gICAgICAgICAgbGV0IGxpbmtFbGVtZW50ID0gZ2V0U2VsZWN0ZWRMaW5rKGVkaXRvcik7XG4gICAgICAgICAgbGV0IGxpbmtUZXh0ID0gJyc7XG4gICAgICAgICAgLy8gU2V0IGV4aXN0aW5nIHZhbHVlcyBiYXNlZCBvbiBzZWxlY3RlZCBlbGVtZW50LlxuICAgICAgICAgIGxldCBleGlzdGluZ1ZhbHVlcyA9IHt9O1xuICAgICAgICAgIGlmIChsaW5rRWxlbWVudCAmJiBsaW5rRWxlbWVudC4kKSB7XG4gICAgICAgICAgICBleGlzdGluZ1ZhbHVlcyA9IHBhcnNlQXR0cmlidXRlcyhlZGl0b3IsIGxpbmtFbGVtZW50KTtcbiAgICAgICAgICAgIGV4aXN0aW5nVmFsdWVzLnNlbGVjdGVkVGV4dCA9IGxpbmtFbGVtZW50LiQuaW5uZXJUZXh0O1xuICAgICAgICAgIH1cbiAgICAgICAgICAvLyBPciwgaWYgYW4gaW1hZ2Ugd2lkZ2V0IGlzIGZvY3VzZWQsIHdlJ3JlIGVkaXRpbmcgYSBsaW5rIHdyYXBwaW5nXG4gICAgICAgICAgLy8gYW4gaW1hZ2Ugd2lkZ2V0LlxuICAgICAgICAgIGVsc2UgaWYgKGZvY3VzZWRMaW5rYWJsZVdpZGdldCAmJiBmb2N1c2VkTGlua2FibGVXaWRnZXQuZGF0YS5saW5rKSB7XG4gICAgICAgICAgICBleGlzdGluZ1ZhbHVlcyA9IENLRURJVE9SLnRvb2xzLmNsb25lKFxuICAgICAgICAgICAgICBmb2N1c2VkTGlua2FibGVXaWRnZXQuZGF0YS5saW5rLFxuICAgICAgICAgICAgKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgLy8gT3Igd2UncmUgY3JlYXRpbmcgYSBuZXcgbGluay5cbiAgICAgICAgICBlbHNlIGlmICh0eXBlb2YgZWRpdG9yLmdldFNlbGVjdGlvbigpLmdldFNlbGVjdGVkVGV4dCgpID09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgICBleGlzdGluZ1ZhbHVlcy5zZWxlY3RlZFRleHQgPSBlZGl0b3IuZ2V0U2VsZWN0aW9uKCkuZ2V0U2VsZWN0ZWRUZXh0KCk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgLy8gUmV0cmlldmUgYW5jaG9ycyBmcm9tIHRoZSBsaW5rIHBsdWdpbi5cbiAgICAgICAgICBsZXQgbGlua1BsdWdpbiA9IENLRURJVE9SLnBsdWdpbnMubGluaztcbiAgICAgICAgICBleGlzdGluZ1ZhbHVlcy5hbmNob3JzID0gbGlua1BsdWdpbi5nZXRFZGl0b3JBbmNob3JzKGVkaXRvcik7XG5cbiAgICAgICAgICAvLyBQcmVwYXJlIGEgc2F2ZSBjYWxsYmFjayB0byBiZSB1c2VkIHVwb24gc2F2aW5nIHRoZSBkaWFsb2cuXG4gICAgICAgICAgY29uc3Qgc2F2ZUNhbGxiYWNrID0gZnVuY3Rpb24ocmV0dXJuVmFsdWVzKSB7XG4gICAgICAgICAgICAvLyBJZiBhbiBpbWFnZSB3aWRnZXQgaXMgZm9jdXNlZCwgd2UncmUgbm90IGVkaXRpbmcgYW4gaW5kZXBlbmRlbnRcbiAgICAgICAgICAgIC8vIGxpbmssIGJ1dCB3ZSdyZSB3cmFwcGluZyBhbiBpbWFnZSB3aWRnZXQgaW4gYSBsaW5rLlxuICAgICAgICAgICAgaWYgKGZvY3VzZWRMaW5rYWJsZVdpZGdldCkge1xuICAgICAgICAgICAgICBmb2N1c2VkTGlua2FibGVXaWRnZXQuc2V0RGF0YShcbiAgICAgICAgICAgICAgICAnbGluaycsXG4gICAgICAgICAgICAgICAgQ0tFRElUT1IudG9vbHMuZXh0ZW5kKFxuICAgICAgICAgICAgICAgICAgcmV0dXJuVmFsdWVzLmF0dHJpYnV0ZXMsXG4gICAgICAgICAgICAgICAgICBmb2N1c2VkTGlua2FibGVXaWRnZXQuZGF0YS5saW5rLFxuICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgIGVkaXRvci5maXJlKCdzYXZlU25hcHNob3QnKTtcbiAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBlZGl0b3IuZmlyZSgnc2F2ZVNuYXBzaG90Jyk7XG5cbiAgICAgICAgICAgIC8vIFNob3J0ZW4gbWFpbHRvIGFuZCB0ZWwgdXJscyBmb3IgdXNlIGFzIGxpbmsgdGV4dCBpZiBub25lIGlzIHByb3ZpZGVkLlxuICAgICAgICAgICAgY29uc3QgbGlua1VybCA9IHJldHVyblZhbHVlcy5hdHRyaWJ1dGVzLmhhc093blByb3BlcnR5KCdocmVmJylcbiAgICAgICAgICAgICAgPyByZXR1cm5WYWx1ZXMuYXR0cmlidXRlcy5ocmVmLnJlcGxhY2UoL15tYWlsdG86LywgJycpLnJlcGxhY2UoL150ZWw6LywgJycpXG4gICAgICAgICAgICAgIDogJyc7XG4gICAgICAgICAgICBjb25zdCBzdWJtaXR0ZWRMaW5rVGV4dCA9IHJldHVyblZhbHVlcy5oYXNPd25Qcm9wZXJ0eSgnbGlua1RleHQnKSAmJiByZXR1cm5WYWx1ZXMubGlua1RleHQgIT09ICcnXG4gICAgICAgICAgICAgID8gcmV0dXJuVmFsdWVzLmxpbmtUZXh0XG4gICAgICAgICAgICAgIDogbGlua1VybDtcblxuICAgICAgICAgICAgLy8gQ3JlYXRlIGEgbmV3IGxpbmsgZWxlbWVudCBpZiBuZWVkZWQuXG4gICAgICAgICAgICBpZiAoIWxpbmtFbGVtZW50ICYmIHJldHVyblZhbHVlcy5hdHRyaWJ1dGVzLmhyZWYpIHtcbiAgICAgICAgICAgICAgY29uc3Qgc2VsZWN0aW9uID0gZWRpdG9yLmdldFNlbGVjdGlvbigpO1xuICAgICAgICAgICAgICBjb25zdCByYW5nZSA9IHNlbGVjdGlvbi5nZXRSYW5nZXMoMSlbMF07XG5cbiAgICAgICAgICAgICAgLy8gVXNlIGxpbmsgVVJMIGFzIHRleHQgd2l0aCBhIGNvbGxhcHNlZCBjdXJzb3IuXG4gICAgICAgICAgICAgIGlmIChyYW5nZS5jb2xsYXBzZWQpIHsgLy9zZWxlY3Rpb24gc3RhcnQ9ZW5kXG4gICAgICAgICAgICAgICAgY29uc3QgdGV4dCA9IG5ldyBDS0VESVRPUi5kb20udGV4dChcbiAgICAgICAgICAgICAgICAgIHN1Ym1pdHRlZExpbmtUZXh0LFxuICAgICAgICAgICAgICAgICAgZWRpdG9yLmRvY3VtZW50LFxuICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgcmFuZ2UuaW5zZXJ0Tm9kZSh0ZXh0KTtcbiAgICAgICAgICAgICAgICByYW5nZS5zZWxlY3ROb2RlQ29udGVudHModGV4dCk7XG4gICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAvLyBDcmVhdGUgdGhlIG5ldyBsaW5rIGJ5IGFwcGx5aW5nIGEgc3R5bGUgdG8gdGhlIG5ldyB0ZXh0LlxuICAgICAgICAgICAgICBjb25zdCBzdHlsZSA9IG5ldyBDS0VESVRPUi5zdHlsZSh7XG4gICAgICAgICAgICAgICAgZWxlbWVudDogJ2EnLFxuICAgICAgICAgICAgICAgIGF0dHJpYnV0ZXM6IHJldHVyblZhbHVlcy5hdHRyaWJ1dGVzLFxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgc3R5bGUudHlwZSA9IENLRURJVE9SLlNUWUxFX0lOTElORTtcbiAgICAgICAgICAgICAgc3R5bGUuYXBwbHlUb1JhbmdlKHJhbmdlKTtcbiAgICAgICAgICAgICAgcmFuZ2Uuc2VsZWN0KCk7XG5cbiAgICAgICAgICAgICAgLy8gU2V0IHRoZSBsaW5rIHNvIGluZGl2aWR1YWwgcHJvcGVydGllcyBtYXkgYmUgc2V0IGJlbG93LlxuICAgICAgICAgICAgICBsaW5rRWxlbWVudCA9IGdldFNlbGVjdGVkTGluayhlZGl0b3IpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy8gVXBkYXRlIHRoZSBsaW5rIHByb3BlcnRpZXMuXG4gICAgICAgICAgICBlbHNlIGlmIChsaW5rRWxlbWVudCkge1xuICAgICAgICAgICAgICBPYmplY3Qua2V5cyhyZXR1cm5WYWx1ZXMuYXR0cmlidXRlcyB8fCB7fSkuZm9yRWFjaChhdHRyTmFtZSA9PiB7XG4gICAgICAgICAgICAgICAgLy8gVXBkYXRlIHRoZSBwcm9wZXJ0eSBpZiBhIHZhbHVlIGlzIHNwZWNpZmllZC5cbiAgICAgICAgICAgICAgICBpZiAocmV0dXJuVmFsdWVzLmF0dHJpYnV0ZXNbYXR0ck5hbWVdLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICAgIGNvbnN0IHZhbHVlID0gcmV0dXJuVmFsdWVzLmF0dHJpYnV0ZXNbYXR0ck5hbWVdO1xuICAgICAgICAgICAgICAgICAgbGlua0VsZW1lbnQuZGF0YShgY2tlLXNhdmVkLSR7YXR0ck5hbWV9YCwgdmFsdWUpO1xuICAgICAgICAgICAgICAgICAgbGlua0VsZW1lbnQuc2V0QXR0cmlidXRlKGF0dHJOYW1lLCB2YWx1ZSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC8vIERlbGV0ZSB0aGUgcHJvcGVydHkgaWYgc2V0IHRvIGFuIGVtcHR5IHN0cmluZy5cbiAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgIGxpbmtFbGVtZW50LnJlbW92ZUF0dHJpYnV0ZShhdHRyTmFtZSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC8vIFVwZGF0ZSB0aGUgbGluayB0ZXh0IGlmIGNoYW5nZWQuXG4gICAgICAgICAgICAgICAgY29uc3QgaXNEaXNwbGF5Q2hhbmdlZCA9IHN1Ym1pdHRlZExpbmtUZXh0ICYmIHN1Ym1pdHRlZExpbmtUZXh0ICE9PSBsaW5rRWxlbWVudC5nZXRIdG1sKCk7XG4gICAgICAgICAgICAgICAgaWYgKGlzRGlzcGxheUNoYW5nZWQpIHtcbiAgICAgICAgICAgICAgICAgbGlua0VsZW1lbnQuc2V0SHRtbChzdWJtaXR0ZWRMaW5rVGV4dCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLy8gU2F2ZSBzbmFwc2hvdCBmb3IgdW5kbyBzdXBwb3J0LlxuICAgICAgICAgICAgZWRpdG9yLmZpcmUoJ3NhdmVTbmFwc2hvdCcpO1xuICAgICAgICAgIH07XG4gICAgICAgICAgLy8gRHJ1cGFsLnQoKSB3aWxsIG5vdCB3b3JrIGluc2lkZSBDS0VkaXRvciBwbHVnaW5zIGJlY2F1c2UgQ0tFZGl0b3JcbiAgICAgICAgICAvLyBsb2FkcyB0aGUgSmF2YVNjcmlwdCBmaWxlIGluc3RlYWQgb2YgRHJ1cGFsLiBQdWxsIHRyYW5zbGF0ZWRcbiAgICAgICAgICAvLyBzdHJpbmdzIGZyb20gdGhlIHBsdWdpbiBzZXR0aW5ncyB0aGF0IGFyZSB0cmFuc2xhdGVkIHNlcnZlci1zaWRlLlxuICAgICAgICAgIGNvbnN0IGRpYWxvZ1NldHRpbmdzID0ge1xuICAgICAgICAgICAgdGl0bGU6IGxpbmtFbGVtZW50XG4gICAgICAgICAgICAgID8gZWRpdG9yLmNvbmZpZy5jaGFpbmxpbmtfZGlhbG9nVGl0bGVFZGl0XG4gICAgICAgICAgICAgIDogZWRpdG9yLmNvbmZpZy5jaGFpbmxpbmtfZGlhbG9nVGl0bGVBZGQsXG4gICAgICAgICAgICBkaWFsb2dDbGFzczogJ2VkaXRvci1jaGFpbmxpbmstZGlhbG9nJyxcbiAgICAgICAgICB9O1xuXG4gICAgICAgICAgLy8gT3BlbiB0aGUgZGlhbG9nIGZvciB0aGUgZWRpdCBmb3JtLlxuICAgICAgICAgIERydXBhbC5ja2VkaXRvci5vcGVuRGlhbG9nKFxuICAgICAgICAgICAgZWRpdG9yLFxuICAgICAgICAgICAgRHJ1cGFsLnVybChgZWRpdG9yL2RpYWxvZy9jaGFpbmxpbmsvJHtlZGl0b3IuY29uZmlnLmRydXBhbC5mb3JtYXR9YCksXG4gICAgICAgICAgICBleGlzdGluZ1ZhbHVlcyxcbiAgICAgICAgICAgIHNhdmVDYWxsYmFjayxcbiAgICAgICAgICAgIGRpYWxvZ1NldHRpbmdzLFxuICAgICAgICAgICk7XG4gICAgICAgIH0sXG4gICAgICB9KTtcbiAgICAgIC8vIGVkaXRvci5hZGRDb21tYW5kKCdkcnVwYWx1bmxpbmsnLCB7XG4gICAgICAvLyAgIGNvbnRleHRTZW5zaXRpdmU6IDEsXG4gICAgICAvLyAgIHN0YXJ0RGlzYWJsZWQ6IDEsXG4gICAgICAvLyAgIHJlcXVpcmVkQ29udGVudDogbmV3IENLRURJVE9SLnN0eWxlKHtcbiAgICAgIC8vICAgICBlbGVtZW50OiAnYScsXG4gICAgICAvLyAgICAgYXR0cmlidXRlczoge1xuICAgICAgLy8gICAgICAgaHJlZjogJycsXG4gICAgICAvLyAgICAgfSxcbiAgICAgIC8vICAgfSksXG4gICAgICAvLyAgIGV4ZWMoZWRpdG9yKSB7XG4gICAgICAvLyAgICAgY29uc3Qgc3R5bGUgPSBuZXcgQ0tFRElUT1Iuc3R5bGUoe1xuICAgICAgLy8gICAgICAgZWxlbWVudDogJ2EnLFxuICAgICAgLy8gICAgICAgdHlwZTogQ0tFRElUT1IuU1RZTEVfSU5MSU5FLFxuICAgICAgLy8gICAgICAgYWx3YXlzUmVtb3ZlRWxlbWVudDogMSxcbiAgICAgIC8vICAgICB9KTtcbiAgICAgIC8vICAgICBlZGl0b3IucmVtb3ZlU3R5bGUoc3R5bGUpO1xuICAgICAgLy8gICB9LFxuICAgICAgLy8gICByZWZyZXNoKGVkaXRvciwgcGF0aCkge1xuICAgICAgLy8gICAgIGNvbnN0IGVsZW1lbnQgPVxuICAgICAgLy8gICAgICAgcGF0aC5sYXN0RWxlbWVudCAmJiBwYXRoLmxhc3RFbGVtZW50LmdldEFzY2VuZGFudCgnYScsIHRydWUpO1xuICAgICAgLy8gICAgIGlmIChcbiAgICAgIC8vICAgICAgIGVsZW1lbnQgJiZcbiAgICAgIC8vICAgICAgIGVsZW1lbnQuZ2V0TmFtZSgpID09PSAnYScgJiZcbiAgICAgIC8vICAgICAgIGVsZW1lbnQuZ2V0QXR0cmlidXRlKCdocmVmJykgJiZcbiAgICAgIC8vICAgICAgIGVsZW1lbnQuZ2V0Q2hpbGRDb3VudCgpXG4gICAgICAvLyAgICAgKSB7XG4gICAgICAvLyAgICAgICB0aGlzLnNldFN0YXRlKENLRURJVE9SLlRSSVNUQVRFX09GRik7XG4gICAgICAvLyAgICAgfSBlbHNlIHtcbiAgICAgIC8vICAgICAgIHRoaXMuc2V0U3RhdGUoQ0tFRElUT1IuVFJJU1RBVEVfRElTQUJMRUQpO1xuICAgICAgLy8gICAgIH1cbiAgICAgIC8vICAgfSxcbiAgICAgIC8vIH0pO1xuXG4gICAgICAvLyBDVFJMICsgSy5cbiAgICAgIGVkaXRvci5zZXRLZXlzdHJva2UoQ0tFRElUT1IuQ1RSTCArIDc1LCAnY2hhaW5saW5rJyk7XG5cbiAgICAgIC8vIEFkZCBidXR0b25zIGZvciBsaW5rIGFuZCB1bmxpbmsuXG4gICAgICBpZiAoZWRpdG9yLnVpLmFkZEJ1dHRvbikge1xuICAgICAgICBlZGl0b3IudWkuYWRkQnV0dG9uKCdDaGFpbmxpbmsnLCB7XG4gICAgICAgICAgbGFiZWw6IERydXBhbC50KCdMaW5rJyksXG4gICAgICAgICAgY29tbWFuZDogJ2NoYWlubGluaycsXG4gICAgICAgIH0pO1xuICAgICAgICAvLyBlZGl0b3IudWkuYWRkQnV0dG9uKCdEcnVwYWxVbmxpbmsnLCB7XG4gICAgICAgIC8vICAgbGFiZWw6IERydXBhbC50KCdVbmxpbmsnKSxcbiAgICAgICAgLy8gICBjb21tYW5kOiAnZHJ1cGFsdW5saW5rJyxcbiAgICAgICAgLy8gfSk7XG4gICAgICB9XG5cbiAgICAgIGVkaXRvci5vbignZG91YmxlY2xpY2snLCBldnQgPT4ge1xuICAgICAgICBjb25zdCBlbGVtZW50ID0gZ2V0U2VsZWN0ZWRMaW5rKGVkaXRvcikgfHwgZXZ0LmRhdGEuZWxlbWVudDtcblxuICAgICAgICBpZiAoIWVsZW1lbnQuaXNSZWFkT25seSgpKSB7XG4gICAgICAgICAgaWYgKGVsZW1lbnQuaXMoJ2EnKSkge1xuICAgICAgICAgICAgZWRpdG9yLmdldFNlbGVjdGlvbigpLnNlbGVjdEVsZW1lbnQoZWxlbWVudCk7XG4gICAgICAgICAgICBlZGl0b3IuZ2V0Q29tbWFuZCgnY2hhaW5saW5rJykuZXhlYygpO1xuICAgICAgICAgICAgaWYoIGVsZW1lbnQuZ2V0QXR0cmlidXRlKCAnbmFtZScgKSAmJiAoICFlbGVtZW50LmdldEF0dHJpYnV0ZSggJ2hyZWYnICkgfHwgIWVsZW1lbnQuZ2V0Q2hpbGRDb3VudCgpICkgKSB7XG4gICAgICAgICAgICAgIC8vIFRoaXMgaXMgYW4gYW5jaG9yLCBkbyBub3RoaW5nLlxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgLy8gUHJldmVudCBvdGhlciBsaW5rIGRpYWxvZ3MuXG4gICAgICAgICAgICAgIGV2dC5zdG9wKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9LCBudWxsLCBudWxsLCAtMTAgKTtcblxuICAgICAgLy8gQWRkIHRoZSBtZW51IGdyb3VwLlxuICAgICAgZWRpdG9yLmFkZE1lbnVHcm91cCgnY2hhaW5saW5rJyk7XG4gICAgICAvLyBJZiB0aGUgXCJtZW51XCIgcGx1Z2luIGlzIGxvYWRlZCwgcmVnaXN0ZXIgdGhlIG1lbnUgaXRlbXMuXG4gICAgICBpZiAoZWRpdG9yLmFkZE1lbnVJdGVtcykge1xuICAgICAgICAvLyBSZW1vdmUgdGhlIGxpbmsgbWVudSBpdGVtcyBjYXVzaWduIGR1cGxpY2F0ZXMuXG4gICAgICAgIC8vIFRPRE86IGh0dHBzOi8vd3d3LmRydXBhbC5vcmcvY29tbWVudC9yZXBseS8zMTMxMjg3XG4gICAgICAgIGVkaXRvci5yZW1vdmVNZW51SXRlbSgnbGluaycpO1xuICAgICAgICBlZGl0b3IucmVtb3ZlTWVudUl0ZW0oJ3VubGluaycpO1xuXG4gICAgICAgIGVkaXRvci5hZGRNZW51SXRlbXMoe1xuICAgICAgICAgIGNoYWlubGluazoge1xuICAgICAgICAgICAgbGFiZWw6IERydXBhbC50KCdFZGl0IExpbmsnKSxcbiAgICAgICAgICAgIGNvbW1hbmQ6ICdjaGFpbmxpbmsnLFxuICAgICAgICAgICAgZ3JvdXA6ICdjaGFpbmxpbmsnLFxuICAgICAgICAgICAgb3JkZXI6IDEsXG4gICAgICAgICAgfSxcbiAgICAgICAgICB1bmNoYWlubGluazoge1xuICAgICAgICAgICAgbGFiZWw6IERydXBhbC50KCdVbmxpbmsnKSxcbiAgICAgICAgICAgIGNvbW1hbmQ6ICd1bmxpbmsnLFxuICAgICAgICAgICAgZ3JvdXA6ICdjaGFpbmxpbmsnLFxuICAgICAgICAgICAgb3JkZXI6IDUsXG4gICAgICAgICAgfSxcbiAgICAgICAgfSk7XG4gICAgICB9XG5cbiAgICAgIC8vIElmIHRoZSBcImNvbnRleHRtZW51XCIgcGx1Z2luIGlzIGxvYWRlZCwgcmVnaXN0ZXIgdGhlIGxpc3RlbmVycy5cbiAgICAgIGlmIChlZGl0b3IuY29udGV4dE1lbnUpIHtcbiAgICAgICAgZWRpdG9yLmNvbnRleHRNZW51LmFkZExpc3RlbmVyKChlbGVtZW50LCBzZWxlY3Rpb24pID0+IHtcbiAgICAgICAgICBpZiAoIWVsZW1lbnQgfHwgZWxlbWVudC5pc1JlYWRPbmx5KCkpIHtcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICAgIH1cbiAgICAgICAgICBjb25zdCBhbmNob3IgPSBnZXRTZWxlY3RlZExpbmsoZWRpdG9yKTtcbiAgICAgICAgICBpZiAoIWFuY2hvcikge1xuICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgbGV0IG1lbnUgPSB7fTtcbiAgICAgICAgICBpZiAoYW5jaG9yLmdldEF0dHJpYnV0ZSgnaHJlZicpICYmIGFuY2hvci5nZXRDaGlsZENvdW50KCkpIHtcbiAgICAgICAgICAgIG1lbnUgPSB7XG4gICAgICAgICAgICAgIGNoYWlubGluazogQ0tFRElUT1IuVFJJU1RBVEVfT0ZGLFxuICAgICAgICAgICAgICB1bmNoYWlubGluazogQ0tFRElUT1IuVFJJU1RBVEVfT0ZGLFxuICAgICAgICAgICAgfTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIG1lbnU7XG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH0sXG4gIH0pO1xuICBDS0VESVRPUi5vbignaW5zdGFuY2VSZWFkeScsIGZ1bmN0aW9uIChlKSB7XG4gIH0pO1xuICAvLyBFeHBvc2UgYW4gQVBJIGZvciBvdGhlciBwbHVnaW5zIHRvIGludGVyYWN0IHdpdGggY2hhaW5saW5rIHdpZGdldHMuXG4gIC8vIChDb21wYXRpYmxlIHdpdGggdGhlIG9mZmljaWFsIENLRWRpdG9yIGxpbmsgcGx1Z2luJ3MgQVBJOlxuICAvLyBodHRwOi8vZGV2LmNrZWRpdG9yLmNvbS90aWNrZXQvMTM4ODUuKVxuICBDS0VESVRPUi5wbHVnaW5zLmNoYWlubGluayA9IHtcbiAgICBwYXJzZUxpbmtBdHRyaWJ1dGVzOiBwYXJzZUF0dHJpYnV0ZXMsXG4gICAgZ2V0TGlua0F0dHJpYnV0ZXM6IGdldEF0dHJpYnV0ZXMsXG4gICAgcmVnaXN0ZXJMaW5rYWJsZVdpZGdldCxcbiAgfTtcbn0pKGpRdWVyeSwgRHJ1cGFsLCBkcnVwYWxTZXR0aW5ncywgQ0tFRElUT1IpO1xuIl19
