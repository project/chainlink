const gulp = require('gulp');
const globBase = require('glob-base');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');
const babel = require('gulp-babel');
const logger = require('fancy-log');
const sourcemaps = require('gulp-sourcemaps');
const processDirs = ['.'];

// ES6 (or JSX) to standard ES5 compliant Javascript.
const jsConfig = {
  src: 'ckeditor_plugins/**/*.es6.js',
  dst: 'ckeditor_plugins',
  babel: {
    presets: ['env', 'react'],
    plugins: ['transform-object-rest-spread'],
    minified: false,
    compact: false,
  },
};

// =============================================
// Utility functions
// =============================================

/**
 * Convert a general glob string, into one aware of the current cwd.
 *
 * @param {string} src
 *   The source Glob string.
 * @param {string} cwd
 *   The current working directory path.
 *
 * @return {object}
 *   An object that contains the glob options as named values in an object.
 */
function resolveGlobOpts(src, cwd) {
  const globOpts = globBase(src);

  return {
    base: `${cwd}/${globOpts.base.replace(/^\/+|\/+$/g, '')}`,
    cwd,
  };
}

/**
 * Runs a callback (Gulp task) with a source glob, using different cwd contexts.
 *
 * @param {string[]} cwds
 *   An array of strings for each of the cwd's to apply the callback with.
 * @param {string} src
 *   The source glob string to use as the source glob.
 * @param {Function} callback
 *   The callback to apply for each of the globs with each of the cwds contexts.
 *
 * @return {Stream|null}
 *   Merged streams of all the Gulp task returned streams. Allows all the
 *   stream values from the applied callback functions to return a single
 *   watchable stream.
 */
function createAllCwdFunc(cwds, src, callback) {
  return (cb) => {
    const reducer = (ds, cwd) => {
      const stream = callback(src, cwd);

      if (!ds) return stream;
      return stream ? merge(ds, stream) : ds;
    };

    const stream = cwds.reduce(reducer, null);
    if (stream) return stream;

    // Signal completion, if no streams were returned.
    cb();
  };
}


// =============================================
// JavaScript building functions
// =============================================

function jsBuildCompile(glob, cwd = '.') {
  const { babel: babelOpts } = jsConfig;
  const renameCallback = (filepath) => {
    filepath.basename = filepath.basename.replace(/(\.es6|\.min){1,2}(\.js)?$/, '');
    filepath.extname = '.js';
  };

  return gulp.src(glob, resolveGlobOpts(jsConfig.src, cwd))
    .pipe(sourcemaps.init())
    .pipe(babel(babelOpts)).on('error', err => logger.error(err))
    .pipe(rename(renameCallback))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(jsConfig.dst, { cwd }))
    .pipe(rename({ extname: '.js' }))
    // .pipe(uglify())
    .pipe(gulp.dest(jsConfig.dst, { cwd }));
}

gulp.task('compile:js', createAllCwdFunc(processDirs, jsConfig.src, jsBuildCompile));

gulp.task('build', gulp.series('compile:js'));
