<?php

namespace Drupal\chainlink_wysiwyg\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\editor\Ajax\EditorDialogSave;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\editor\Form\EditorLinkDialog;

/**
 * Provides a link dialog for text editors.
 *
 * @internal
 */
class ChainlinkDialog extends EditorLinkDialog {

  const URL_KEY = 'url';
  const ANCHOR_KEY = 'anchor';
  const EMAIL_KEY = 'email';
  const PHONE_KEY = 'phone';
  const TEL_PREFIX = 'tel:+1 ';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return parent::getFormId(); //'editor_chainlink_dialog';
  }

  /**
   * Helper function that returns an array of link types, keyed by the
   * constant identifier.
   *
   * @return array
   */
  protected function getLinkTypeOptions() {
    return [
      self::URL_KEY => $this->t('URL'),
      self::ANCHOR_KEY => $this->t('Anchor in page'),
      self::EMAIL_KEY => $this->t('Email'),
      self::PHONE_KEY => $this->t('Phone'),
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\editor\Entity\Editor $editor
   *   The text editor to which this dialog corresponds.
   */
  public function buildForm(array $form, FormStateInterface $form_state, Editor $editor = NULL) {
    $form = parent::buildForm($form, $form_state, $editor);

    // The default values are set directly from \Drupal::request()->request,
    // provided by the editor plugin opening the dialog.
    $user_input = $form_state->getUserInput();
    $input = isset($user_input['editor_object']) ? $user_input['editor_object'] : [];
    $options = $this->getLinkTypeOptions();
    $hrefOriginal = isset($input['href']) ? $input['href'] : '';
    $linkTypeOriginal = $this->identifyLinkType($hrefOriginal);
    $hrefInfo = parse_url($hrefOriginal);

    // The 'selectedText' parameter will not be passes of objects like images.
    if (isset($input['selectedText'])) {
      $form['linkText'] = [
        '#title' => $this->t('Display Text'),
        '#type' => 'textfield',
        '#default_value' => $input['selectedText'] ?: '' ,
        '#weight' => -199,
      ];
    }

    $form['link_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Link Type'),
      '#weight' => -200,
      '#options' => $options,
      '#default_value' => $linkTypeOriginal,
    ];

    // -----------------------
    // Default Url form field.
    // -----------------------
    // This field must stay here in the form structure to maintain compatibility
    // with Linkit `hook_form_alter` functions and the EditorDialogSave command.
    $form['attributes']['href']['#states'] = [
      'enabled' => [
        ':input[name="link_type"]' => ['value' => self::URL_KEY]
      ],
      'visible' => [
        ':input[name="link_type"]' => ['value' => self::URL_KEY]
      ]
    ];

    $form['href_original'] = [
      '#type' => 'hidden',
      '#value' => $hrefOriginal,
    ];

    // Don't display the raw url in the form if not a url link type.
    if ($linkTypeOriginal !== self::URL_KEY) {
      $form['attributes']['href']['#value'] = '';
    }

    $form['subform'] = [
      '#type' => 'container',
      '#prefix' => '<div id="chainlink-subform">',
      '#suffix' => '</div>'
    ];


    // -----------------------
    // Telephone form fields
    // -----------------------
    $form['subform'][self::PHONE_KEY] = [
      '#type' => 'container',
      '#states' => [
        'enabled' => [
          ':input[name="link_type"]' => ['value' => self::PHONE_KEY]
        ],
        'visible' => [
          ':input[name="link_type"]' => ['value' => self::PHONE_KEY]
        ]
      ],
      'phone_number' => [
        '#type' => 'tel',
        '#title' => $this->t('Phone Number'),
      ]
    ];

    // Set the default value if this is a Phone link.
    if ($linkTypeOriginal === self::PHONE_KEY) {
      if (isset($hrefInfo['path'])) {
        $form['subform'][self::PHONE_KEY]['phone_number']['#default_value'] = self::formatPhoneNumber($hrefOriginal);
      }
    }

    // -----------------------
    // Email form fields
    // -----------------------
    $form['subform'][self::EMAIL_KEY] = [
      '#type' => 'container',
      '#states' => [
        'enabled' => [
          ':input[name="link_type"]' => ['value' => self::EMAIL_KEY]
        ],
        'visible' => [
          ':input[name="link_type"]' => ['value' => self::EMAIL_KEY]
        ]
      ],
      'email_address' => [
        '#type' => 'email',
        '#title' => $this->t('E-mail Address'),
      ],
      'email_subject' => [
        '#type' => 'textfield',
        '#title' => $this->t('Message Subject')
      ],
      'email_body' => [
        '#type' => 'textarea',
        '#title' => $this->t('Message Body'),
      ],
    ];

    // Set the default values if this is a Email link.
    if ($linkTypeOriginal === self::EMAIL_KEY) {
      if (isset($hrefInfo['path'])) {
        $form['subform'][self::EMAIL_KEY]['email_address']['#default_value'] = $hrefInfo['path'];

        $emailParameters = [];
        if (isset($hrefInfo['query'])) {
         parse_str($hrefInfo['query'], $emailParameters);
        }

        $subject = isset($emailParameters['subject']) ? $emailParameters['subject'] : '';
        $form['subform'][self::EMAIL_KEY]['email_subject']['#default_value'] = rawurldecode($subject);


        $body = isset($emailParameters['subject']) ? $emailParameters['body'] : '';
        $form['subform'][self::EMAIL_KEY]['email_body']['#default_value'] = rawurldecode($body);
      }
    }

    // -----------------------
    // Anchor form fields
    // -----------------------
    // Get any anchors passed by the plugin.
    $anchors = [];
    if (!empty($input['anchors'])) {
      foreach ($input['anchors'] as $a) {
        $anchors[$a['id']] = $a['name'];
      }
    }

    $form['subform'][self::ANCHOR_KEY] = [
      '#type' => 'container',
      '#states' => [
        'enabled' => [
          ':input[name="link_type"]' => ['value' => self::ANCHOR_KEY]
        ],
        'visible' => [
          ':input[name="link_type"]' => ['value' => self::ANCHOR_KEY]
        ]
      ],
    ];

    $anchorBuild = [
      '#markup' => $this->t('[ No anchors available in the document ]')
    ];

    // Create select element if anchors found in document.
    if ($anchors) {
      unset($anchorBuild['#markup']);

      $anchorBuild = [
        '#type' => 'select',
        '#title' => $this->t('Select an Anchor'),
        '#options' => $anchors,
      ];
    }

    $form['subform'][self::ANCHOR_KEY]['anchorElement'] = $anchorBuild;
    // Set the default value if this is a Anchor link.
    if ($linkTypeOriginal === self::ANCHOR_KEY) {
      $anchorSelectValue = ltrim($hrefOriginal, '#');
      $form['subform'][self::ANCHOR_KEY]['anchorElement']['#default_value'] = $anchorSelectValue;
    }

    return $form;
  }

  /**
   * Link type select element Ajax callback.
   */
  public function linkTypeCallback(array &$form, FormStateInterface $form_state) {
    if ($selectedValue = $form_state->getValue('link_type')) {
      if (method_exists($this, $selectedValue)) {
        // Reset the subform.
        $form['subform'] = [
          '#type' => 'container',
          '#prefix' => '<div id="chainlink-subform">',
          '#suffix' => '</div>'
        ];
        $form['subform'] += $this->$selectedValue($form, $form_state);
      }
    }
    return $form['subform'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue('subform');
    switch ($form_state->getValue('link_type')) {
      case self::ANCHOR_KEY:
        // Nothing to validate, we will handle 'no-anchors available' in the submit.
        break;
      case self::EMAIL_KEY:
        // Email form field will handle validation if filled.
        if(empty($values[self::EMAIL_KEY]['email_address'])) {
          $form_state->setError($form['subform'][self::EMAIL_KEY]['email_address'], $this->t('Please enter a valid email address.'));
        }
        break;
      case self::PHONE_KEY:
        // Format phone number
        $phoneNumber = $values[self::PHONE_KEY]['phone_number'];
        $phoneNumberDigits = preg_replace('/[^\d]/', '', $phoneNumber);
        if (strlen( (string) $phoneNumberDigits) != 10) {
          $form_state->setError($form['subform'][self::PHONE_KEY]['phone_number'], $this->t('Please enter a valid 10 digit phone number.'));
        }
        // create display text if none
        if ($form_state->isValueEmpty('linkText')) {
          $form_state->setValue('linkText', self::formatPhoneNumber($phoneNumberDigits));
        }
        break;
      default:
        //Do nothing, url field can handle itself.
        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    if ($form_state->getErrors()) {
      unset($form['#prefix'], $form['#suffix']);
      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -10,
      ];
      $response->addCommand(new HtmlCommand('#editor-link-dialog-form', $form));
    }
    else {
      $values = $form_state->getValues();
      $this->massageFormValues($values);

      // Check for null anchors condition before saving/
      $response->addCommand(new EditorDialogSave($values));
      $response->addCommand(new CloseModalDialogCommand());
    }

    return $response;
  }

  protected function massageFormValues(&$values) {
    $subformValues = $values['subform'];
    switch ($values['link_type']) {
      case self::ANCHOR_KEY:
        $values['attributes']['target'] = '_self';
        $values['attributes']['href'] = '#' . $subformValues[self::ANCHOR_KEY]['anchorElement'];
        break;
      case self::EMAIL_KEY:
        $to = $subformValues[self::EMAIL_KEY]['email_address'];
        $subject = $subformValues[self::EMAIL_KEY]['email_subject'];
        $body = $subformValues[self::EMAIL_KEY]['email_body'];
        $href ='mailto:'. $to;
        $params = [];
        if ($subject || $body) {
          if ($subject) {
            $params['subject'] = $subject;
          }
          if ($body) {
            $params['body'] = $body;
          }
          $href .= '?' . http_build_query($params, null, '&', PHP_QUERY_RFC3986);
        }
        $values['attributes']['href'] = $href;

        // Set a default link text for emails.
        $values['linkText'] = empty($values['linkText']) ? $to : $values['linkText'];
        break;
      case self::PHONE_KEY:
        $phoneNumber = $subformValues[self::PHONE_KEY]['phone_number'];
        $values['attributes']['href'] =  self::TEL_PREFIX . self::formatPhoneNumber($phoneNumber);
        break;
      case self::URL_KEY:
      default:
        // Set a default link text for internal links.
        if (empty($values['linkText'])
          && !empty($values['attributes']['href'])
          && !UrlHelper::isExternal($values['attributes']['href'])) {
          $internalRouteParams = Url::fromUri('internal:' . $values['attributes']['href'])->getRouteParameters();
          $routeEntity = NULL;
          foreach ($internalRouteParams as $param) {
            if ($param instanceof EntityInterface) {
              $routeEntity = $param;
              break;
            }
          }
          if (is_null($routeEntity)) {
            // Some routes don't properly define entity parameters.
            // Thus, try to load them by its raw Id, if given.
            $entityTypeManager = \Drupal::entityTypeManager();
            $types = $entityTypeManager->getDefinitions();
            foreach ($internalRouteParams as $paramKey => $param) {
              if (!isset($types[$paramKey])) {
                continue;
              }
              if (is_string($param) || is_numeric($param)) {
                try {
                  $routeEntity = $entityTypeManager->getStorage($paramKey)->load($param);
                } catch (\Exception $e) {}
              }
              break;
            }
          }
          if (!is_null($routeEntity) && $routeEntity instanceof EntityInterface) {
            $values['linkText'] = $routeEntity->getTitle();
          }
        }
      break;
    }

    unset($values['subform']);
  }

  protected static function formatPhoneNumber($phoneNumber) {
    $phoneNumber = preg_replace('/^' . preg_quote(self::TEL_PREFIX, '/') . '/', '', $phoneNumber);
    $phoneNumber = preg_replace('/^\+1\s/', '', $phoneNumber);
    $phoneNumber = preg_replace('/[^\d]/', '', $phoneNumber);

    return '(' . substr($phoneNumber, 0, 3) . ') '
      . substr($phoneNumber, 3, 3)
      . '-' . substr($phoneNumber,6);
  }

  protected function identifyLinkType($linkText) {
    if ($this->strStartsWith($linkText, '#')) {
      return self::ANCHOR_KEY;
    } elseif ($this->strStartsWith($linkText, 'mailto:')) {
      return self::EMAIL_KEY;
    } elseif ($this->strStartsWith($linkText, self::TEL_PREFIX)) {
      return self::PHONE_KEY;
    } else {
      return self::URL_KEY;
    }
  }

  protected function strStartsWith($string, $prefix) {
    return substr($string, 0, strlen($prefix)) === $prefix;
  }
}
