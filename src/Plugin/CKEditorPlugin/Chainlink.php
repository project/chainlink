<?php

namespace Drupal\chainlink_wysiwyg\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginButtonsInterface;
use Drupal\ckeditor\CKEditorPluginContextualInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "chainlink" plugin.
 *
 * @CKEditorPlugin(
 *   id = "chainlink",
 *   label = @Translation("Link combination plugin"),
 *   module = "chainlink_wysiwyg"
 * )
 */
class Chainlink extends ChainlinkCkePluginBase implements CKEditorPluginButtonsInterface {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->getPluginPath() . '/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [
      'core/drupal.ajax',
      'chainlink_wysiwyg/chainlink_wysiwyg.chainlink.styles'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $libraryPath = $this->getPluginPath();
    return [
      'Chainlink' => [
        'label' => $this->t('Chain Link'),
        'image' => $libraryPath . '/icons/chainlink.png',
      ],
//      'Unlink' => [
//        'label' => $this->t('Replace'),
//        'image' => $libraryPath . '/icons/unlink.png',
//      ],
//      'Anchor' => [
//        'label' => $this->t('Anchor'),
//        'image' => $libraryPath . '/icons/anchor.png',
//      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(Editor $editor) {
    //todo, Add enable/disable checkbox to config form
    return TRUE;
  }

}
