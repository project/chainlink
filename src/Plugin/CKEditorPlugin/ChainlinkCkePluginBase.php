<?php

namespace Drupal\chainlink_wysiwyg\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginButtonsInterface;
use Drupal\ckeditor\CKEditorPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines a base CKEditor plugin implementation.
 *
 * No other CKEditor plugins can be internal, unless a different CKEditor build
 * than the one provided by Drupal core is used. Most CKEditor plugins don't
 * need to provide additional settings forms.
 *
 * This base class assumes that your plugin has buttons that you want to be
 * enabled through the toolbar builder UI. It is still possible to also
 * implement the CKEditorPluginContextualInterface (for contextual enabling) and
 * CKEditorPluginConfigurableInterface interfaces (for configuring plugin
 * settings).
 *
 * NOTE: the Drupal plugin ID should correspond to the CKEditor plugin name.
 *
 * @see \Drupal\ckeditor\CKEditorPluginInterface
 * @see \Drupal\ckeditor\CKEditorPluginButtonsInterface
 * @see \Drupal\ckeditor\CKEditorPluginContextualInterface
 * @see \Drupal\ckeditor\CKEditorPluginConfigurableInterface
 * @see \Drupal\ckeditor\CKEditorPluginManager
 * @see \Drupal\ckeditor\Annotation\CKEditorPlugin
 * @see plugin_api
 */
abstract class ChainlinkCkePluginBase extends PluginBase implements CKEditorPluginInterface  {

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->getPluginPath() . '/plugin.js';
  }

  /**
   * Helper function to find ckeditor plugin directory
   */
  protected function getPluginPath($plugin = NULL) {
    // Following the logic in Drupal 8.9.x and Drupal 9.x
    // ----------------------------------------------------------------------
    // Issue #3096648: Add support for third party libraries in site specific
    // and install profile specific libraries folders
    // https://www.drupal.org/project/drupal/issues/3096648
    //
    // https://git.drupalcode.org/project/drupal/commit/1edf15f
    // -----------------------------------------------------------------------

    $pluginId = $plugin ? $plugin : $this->getPluginId();

    // Check this module first for overridden plugin version
    $directories[] = drupal_get_path('module', 'chainlink_wysiwyg') . '/ckeditor_plugins/';

    // Search sites/<domain>/*.
    $directories[] = \Drupal::service('site.path') . "/libraries/";

    //Prefer plugins here over the rool libraries folder
    $directories[] = 'libraries/ckeditor/plugins/';

    // Always search the root 'libraries' directory.
    $directories[] = 'libraries/';

    // Installation profiles can place libraries into a 'libraries' directory.
    if ($installProfile = \Drupal::installProfile()) {
      $profile_path = drupal_get_path('profile', $installProfile);
      $directories[] = "$profile_path/libraries/";
    }

    foreach ($directories as $dir) {
      if (file_exists(DRUPAL_ROOT . '/' . $dir . $pluginId.'/plugin.js')) {
        return $dir . $pluginId;
      }
    }

    // TODO: Log a watchdog "Library not found" error message here,
    // as there is really nothing here and this will fail silently,
    // aside from possible console errors.
    return 'libraries/' . $pluginId;
  }


}
