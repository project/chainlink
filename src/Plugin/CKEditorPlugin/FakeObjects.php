<?php

namespace Drupal\chainlink_wysiwyg\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginContextualInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "fakeobjects" plugin.
 *
 * @CKEditorPlugin(
 *   id = "fakeobjects",
 *   label = @Translation("FakeObjects"),
 *   module = "medicaid_wysiwyg"
 * )
 */
class FakeObjects extends ChainlinkCkePluginBase implements CKEditorPluginContextualInterface {

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(Editor $editor) {
    //todo, check if any enabled modules depend on this module
    //$settings = $editor->getSettings();

    // For now, its required by footnotes plugin.git status
    return TRUE;
  }

}
