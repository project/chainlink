<?php

namespace Drupal\chainlink_wysiwyg\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginButtonsInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "link" plugin.
 *
 * @CKEditorPlugin(
 *   id = "link",
 *   label = @Translation("CKEditor `Link` plugin"),
 *   module = "chainlink_wysiwyg"
 * )
 */
class Link extends ChainlinkCkePluginBase implements CKEditorPluginButtonsInterface {

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $libraryPath = $this->getPluginPath();
    return [
      'Link' => [
        'label' => $this->t('Anchor Link'),
        'image' => $libraryPath . '/icons/link.png',
      ],
      'Unlink' => [
        'label' => $this->t('Replace'),
        'image' => $libraryPath . '/icons/unlink.png',
      ],
      'Anchor' => [
        'label' => $this->t('Anchor'),
        'image' => $libraryPath . '/icons/anchor.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

}
